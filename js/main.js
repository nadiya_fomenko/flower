$(document).ready(function(){
  $('.menu-in-circle-2').fadeOut();
  $('.menu-in-circle-3').fadeOut();
  $('.menu-in-circle-4').fadeOut();
  $('.menu-in-circle-5').fadeOut();
  $('.menu-in-circle-6').fadeOut();
  $('.menu-in-circle-1').fadeIn();
  $('.curier-block').fadeOut();
  $('.florist-block').fadeIn();
  $('.houseplants').fadeOut();
  $('.fresh-flowers').fadeIn();
  $('#houseplants-back-arrow').fadeOut();
  $('#houseplants-next-arrow').fadeOut();
  $('#fresh-flowers-back-arrow').fadeIn();
  $('#fresh-flowers-next-arrow').fadeIn();
  $('.assesories').fadeOut();
  $('.gifs').fadeOut();
  $('#gifs-back-arrow').fadeOut();
  $('#gifs-next-arrow').fadeOut();
  $('.bouquets').fadeOut();
  $('#bouquets-back-arrow').fadeOut();
  $('#bouquets-next-arrow').fadeOut();
  $('.soil').fadeOut();
  $('#soil-back-arrow').fadeOut();
  $('#soil-next-arrow').fadeOut();
  $('#assesories-back-arrow').fadeOut();
  $('#assesories-next-arrow').fadeOut();
  

          // HERO-MENU
        $('.menu-circle__1').on('click', function(event) {
         event.preventDefault();
          $('.menu-in-circle-2').fadeOut();
          $('.menu-in-circle-3').fadeOut();
          $('.menu-in-circle-4').fadeOut();
          $('.menu-in-circle-5').fadeOut();
          $('.menu-in-circle-6').fadeOut();
          $('.menu-in-circle-1').fadeIn();
          });

        $('.menu-circle__2').on('click',function(event){
          event.preventDefault();
            $('.menu-in-circle-1').fadeOut();
            $('.menu-in-circle-3').fadeOut(); 
            $('.menu-in-circle-4').fadeOut();
            $('.menu-in-circle-5').fadeOut(); 
            $('.menu-in-circle-6').fadeOut();
            $('.menu-in-circle-2').fadeIn();
          });
        $('.menu-circle__3').on('click',function(event){
          event.preventDefault();
            $('.menu-in-circle-1').fadeOut(); 
            $('.menu-in-circle-2').fadeOut();
            $('.menu-in-circle-4').fadeOut(); 
            $('.menu-in-circle-5').fadeOut();
            $('.menu-in-circle-6').fadeOut();
            $('.menu-in-circle-3').fadeIn();
          });
        $('.menu-circle__4').on('click',function(event){
            event.preventDefault();
              $('.menu-in-circle-1').fadeOut();
              $('.menu-in-circle-2').fadeOut();
              $('.menu-in-circle-3').fadeOut(); 
              $('.menu-in-circle-5').fadeOut();
              $('.menu-in-circle-6').fadeOut();
              $('.menu-in-circle-4').fadeIn();
          });

        $('.menu-circle__5').on('click',function(event){
              event.preventDefault();
                $('.menu-in-circle-1').fadeOut();
                $('.menu-in-circle-2').fadeOut();
                $('.menu-in-circle-3').fadeOut(); 
                $('.menu-in-circle-4').fadeOut();
                $('.menu-in-circle-6').fadeOut();
                $('.menu-in-circle-5').fadeIn();
          });
        $('.menu-circle__6').on('click',function(event){
                event.preventDefault();
                  $('.menu-in-circle-1').fadeOut();
                  $('.menu-in-circle-2').fadeOut();
                  $('.menu-in-circle-3').fadeOut(); 
                  $('.menu-in-circle-4').fadeOut();
                  $('.menu-in-circle-5').fadeOut();
                  $('.menu-in-circle-6').fadeIn();
          });


                // ABOUT-MENU
        $('.menu-circle__2-1').on('click',function(event){
                  event.preventDefault();
                  $('.curier-block').fadeOut();
                  $('.florist-block').fadeIn();
                  
            });
        $('.menu-circle__2-2').on('click',function(event){
              event.preventDefault();
              $('.florist-block').fadeOut();
              $('.curier-block').fadeIn();
              
        });
        
        
        // SLICK

        $('.fresh-flowers').slick({
          arrows: true,
          autoplay:true,
          prevArrow: $('#fresh-flowers-back-arrow'),
          nextArrow: $('#fresh-flowers-next-arrow')
        });

        $('.houseplants').slick({
          arrows: true,
          autoplay:true,
          prevArrow: $('#houseplants-back-arrow'),
          nextArrow: $('#houseplants-next-arrow')
        });

        $('.gifs').slick({
          arrows: true,
          autoplay:true,
          prevArrow: $('#gifs-back-arrow'),
          nextArrow: $('#gifs-next-arrow')
        });

        $('.bouquets').slick({
          arrows: true,
          autoplay:true,
          prevArrow: $('#bouquets-back-arrow'),
          nextArrow: $('#bouquets-next-arrow')
        });

        $('.assesories').slick({
          arrows: true,
          autoplay:true,
          prevArrow: $('#assesories-back-arrow'),
          nextArrow: $('#assesories-next-arrow')
        });






        // MENU-OFFER
        $('.menu-circle__3-1').on('click',function(event){
          event.preventDefault();
          $('.gifs').fadeOut();
          $('.houseplants').fadeOut();
          $('.fresh-flowers').fadeIn();
          $('.assesories').fadeOut();
          $('.bouquets').fadeOut();
          $('#bouquets-back-arrow').fadeOut();
          $('#bouquets-next-arrow').fadeOut();
          $('.soil').fadeOut();
          $('#soil-back-arrow').fadeOut();
          $('#soil-next-arrow').fadeOut();

          $('#gifs-back-arrow').fadeOut();
          $('#gifs-next-arrow').fadeOut();
          $('#assesories-back-arrow').fadeOut();
          $('#assesories-next-arrow').fadeOut();
          $('#houseplants-back-arrow').fadeOut();
          $('#houseplants-next-arrow').fadeOut();
          $('#fresh-flowers-back-arrow').fadeIn();
          $('#fresh-flowers-next-arrow').fadeIn();
        });
        $('.menu-circle__3-2').on('click',function(event){
            event.preventDefault();
            $('.gifs').fadeOut();
            $('.fresh-flowers').fadeOut();
            $('.houseplants').fadeIn();
            $('.assesories').fadeOut();
            $('.bouquets').fadeOut();
            $('#bouquets-back-arrow').fadeOut();
            $('#bouquets-next-arrow').fadeOut();
            $('.soil').fadeOut();
            $('#soil-back-arrow').fadeOut();
            $('#soil-next-arrow').fadeOut();

            $('#assesories-back-arrow').fadeOut();
            $('#assesories-next-arrow').fadeOut();
            $('#gifs-back-arrow').fadeOut();
            $('#gifs-next-arrow').fadeOut();
            $('#houseplants-back-arrow').fadeIn();
            $('#houseplants-next-arrow').fadeIn();
            $('#fresh-flowers-back-arrow').fadeOut();
            $('#fresh-flowers-next-arrow').fadeOut();
        });
        $('.menu-circle__3-6').on('click',function(event){
          event.preventDefault();
          $('.gifs').fadeIn();
          $('.fresh-flowers').fadeOut();
          $('.houseplants').fadeOut();
          $('.assesories').fadeOut();
          $('.bouquets').fadeOut();
          $('#bouquets-back-arrow').fadeOut();
          $('#bouquets-next-arrow').fadeOut();
          $('.soil').fadeOut();
          $('#soil-back-arrow').fadeOut();
          $('#soil-next-arrow').fadeOut();

          $('#assesories-back-arrow').fadeOut();
          $('#assesories-next-arrow').fadeOut();
          $('#gifs-back-arrow').fadeIn();
          $('#gifs-next-arrow').fadeIn();
          $('#houseplants-back-arrow').fadeOut();
          $('#houseplants-next-arrow').fadeOut();
          $('#fresh-flowers-back-arrow').fadeOut();
          $('#fresh-flowers-next-arrow').fadeOut();
      });
      $('.menu-circle__3-3').on('click',function(event){
        event.preventDefault();
        $('.gifs').fadeOut();
        $('.fresh-flowers').fadeOut();
        $('.houseplants').fadeOut();
        $('.assesories').fadeOut();
        $('.bouquets').fadeOut();
        $('#bouquets-back-arrow').fadeOut();
        $('#bouquets-next-arrow').fadeOut();
        $('.soil').fadeIn();
        $('#soil-back-arrow').fadeIn();
        $('#soil-next-arrow').fadeIn();
  
        $('#assesories-back-arrow').fadeOut();
        $('#assesories-next-arrow').fadeOut();
        $('#gifs-back-arrow').fadeOut();
        $('#gifs-next-arrow').fadeOut();
        $('#houseplants-back-arrow').fadeOut();
        $('#houseplants-next-arrow').fadeOut();
        $('#fresh-flowers-back-arrow').fadeOut();
        $('#fresh-flowers-next-arrow').fadeOut();
    });
      $('.menu-circle__3-4').on('click',function(event){
        event.preventDefault();
        $('.gifs').fadeOut();
        $('.fresh-flowers').fadeOut();
        $('.houseplants').fadeOut();
        $('.assesories').fadeIn();
        $('.bouquets').fadeOut();
        $('#bouquets-back-arrow').fadeOut();
        $('#bouquets-next-arrow').fadeOut();
        $('.soil').fadeOut();
        $('#soil-back-arrow').fadeOut();
        $('#soil-next-arrow').fadeOut();

        $('#assesories-back-arrow').fadeIn();
        $('#assesories-next-arrow').fadeIn();
        $('#gifs-back-arrow').fadeOut();
        $('#gifs-next-arrow').fadeOut();
        $('#houseplants-back-arrow').fadeOut();
        $('#houseplants-next-arrow').fadeOut();
        $('#fresh-flowers-back-arrow').fadeOut();
        $('#fresh-flowers-next-arrow').fadeOut();
    });

    $('.menu-circle__3-5').on('click',function(event){
      event.preventDefault();
      $('.gifs').fadeOut();
      $('.fresh-flowers').fadeOut();
      $('.houseplants').fadeOut();
      $('.assesories').fadeOut();
      $('.bouquets').fadeIn();
      $('#bouquets-back-arrow').fadeIn();
      $('#bouquets-next-arrow').fadeIn();
      $('.soil').fadeOut();
      $('#soil-back-arrow').fadeOut();
      $('#soil-next-arrow').fadeOut();

      $('#assesories-back-arrow').fadeOut();
      $('#assesories-next-arrow').fadeOut();
      $('#gifs-back-arrow').fadeOut();
      $('#gifs-next-arrow').fadeOut();
      $('#houseplants-back-arrow').fadeOut();
      $('#houseplants-next-arrow').fadeOut();
      $('#fresh-flowers-back-arrow').fadeOut();
      $('#fresh-flowers-next-arrow').fadeOut();
  });
      });     
